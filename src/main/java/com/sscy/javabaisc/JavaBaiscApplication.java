package com.sscy.javabaisc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaBaiscApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaBaiscApplication.class, args);

        boolean mybox1 = true;
        System.out.println(mybox1);

        char mybox2 = 'Q';
        Character mybox222 = 'a';
        String mybox22 = "w";

        byte mybox3 = 1;
        Byte mybox33 = 1;

        short mybox4 = 2;
        int mybox5 = 10;
        long mybox6 = 15L;

        float mybox7 = 11.2f;
        double mybox8 = 123.477;

    }


}
